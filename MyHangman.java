/**
 *  author: Luis Delgado
 *  date:  December 2012
 *  this program lets the user play a game of hangman
 */

import java.util.*;
import java.io.*;
import java.awt.*;

public class MyHangman {

	/**
     *  The main method calls pickWord and stores the random word chosen by the computer in
	 *  the String variable named word. It, then, calls playGame and passes the word chosen
	 *  as a parameter.
     *  <br>
     *  pre: none</br>
     *  post: introduction() will have been called
     */
	public static void main(String[] args) throws IOException{
		DrawingPanel canvas = new DrawingPanel(100,150);
		String word = pickWord("dictionary.txt");
		introduction(word, new Scanner(System.in), canvas);
	}

	/**
     *  This method uses a random object to choose a word from the text file. The first token 
	 *  in the text file is the number of words that it has. It then returns the chosen word.
     *  <br>
     *  pre: s != null </br>
     *  post: none
     *  @param s name of the text file with the words from which to pick
     *  @return the chosen word for the current round
     */
	public static String pickWord(String s) throws IOException{
		Scanner sc = new Scanner(new File(s));
		int words = sc.nextInt();
		Random generator = new Random(); 
		int ran = generator.nextInt(words)+1;
		String word = "";
		for(int i=1;i<=ran;i++) {
			word = sc.next();
		}
		return word;
	}

	/**
     *  This method greets the player and calls playGame to start the game itself.
     *  <br>
     *  pre: word != null; sc != null; canvas != null </br>
     *  post: a dashed String with same length as current word is created and sent to playGame()
     *  @param word current word; sc Scanner; canvas where the Hangman will be drawn
     */
	public static void introduction(String word, Scanner sc, DrawingPanel canvas) throws IOException {
		System.out.println("Welcome to Hangman!\n");
		System.out.println("You are guessing a word of length "+word.length());
		String dash = "";
		for(int i=0;i<word.length();i++) {
			dash+="-";
		}
		playGame(word, dash, sc, canvas);
	}

	/**
	 *  This method plays the actual game; with the help of the methods draw() and compare() it returns a dashed 
	 *  String letting the user know whether or not the letter chosen is in the word or not.    
	 *  <br>
     *  pre: word != null; dash != null; sc != null; canvas != null </br>
     *  post: a dashed String with same length as current word is created and sent to playGame()
     *  @param word current word; dash dashed word; sc Scanner; canvas where the Hangman will be drawn
     */
	public static void playGame(String word, String dash, Scanner sc, DrawingPanel canvas) throws IOException {
		ArrayList<Character> abc = new ArrayList<Character>();
		int wrongGuess = 6;
		int goodGuess = word.length();
		while(goodGuess > 0 && wrongGuess > 0) {
			System.out.print("\nGuess a letter: ");
			String tempLettter = sc.next();
			char actualLetter = tempLettter.charAt(0);
			if(!abc.contains(actualLetter)) 
				abc.add(actualLetter);
			else {
				while(abc.contains(actualLetter)) {
					System.out.println("You have already guessed "+actualLetter);
					System.out.print("\nGuess a letter: ");
					tempLettter = sc.next();
					actualLetter = tempLettter.charAt(0);
				}
				abc.add(actualLetter);
			}
			if(compare(word, actualLetter)) { 
				String oldDash = dash;
				dash = dashChanger(dash, actualLetter, word);
				goodGuess -= hammingDistance(dash, oldDash);
				if(goodGuess == 0)
					askForNewGame(sc);
			}
			else {
				System.out.println("Oops! Letter "+actualLetter+" is not there. Adding to hangman...");
				draw(wrongGuess, canvas);
				wrongGuess--;
				if(wrongGuess==0){
					System.out.println();
					System.out.println("Game over!");
					System.out.println("The word was "+word);
					askForNewGame(sc);
				}
			}
		}
	}

	/**
	 *  returns the hamming distance between the two dashed strings  
	 *  <br>
     *  pre: dash != null; oldDash != null </br>
     *  post: none
     *  @param dash new dashed word; oldDash dashed word as of last guess
     *  @return number of different characters in two equal-length strings
     */
	public static int hammingDistance(String dash, String oldDash) {
		int count = 0;
		for(int i = 0; i < dash.length(); i++) {
			if(dash.charAt(i) != oldDash.charAt(i))
				count++;
		}
		return count;
	}

	/**
	 *  Returns the new dashed string after the user made a guess
	 *  <br>
     *  pre: dash != null; word != null </br>
     *  post: none
     *  @param dash dashed word; letter current user's guess; word current word
     *  @return new dashed string
     */
	public static String dashChanger(String dash, char letter, String word) {
		String newString = "";
		for(int i=0;i<dash.length();i++) {
			if(Character.isLetter(dash.charAt(i)) && (dash.charAt(i)!=letter)) {
				newString+=dash.charAt(i); 
			}
			else if(word.charAt(i) != letter) {
				newString+="-";
			}
			else if(word.charAt(i) == letter){
				newString+=letter;
			}
		}
		System.out.println("Good guess! The word is: "+newString);
		return newString;
	}

	/**
	 *  This method is called to draw the different parts of the hangman.
	 *  <br>
     *  pre: canvas != null </br>
     *  post: none
     *  @param drawBodyPart int representing which part to draw; canvas window where the Hangman is drawn
     */
	public static void draw(int drawBodyPart, DrawingPanel canvas) {
		Graphics g = canvas.getGraphics();
		if(drawBodyPart == 6) {
			g.drawOval(35,32,24,24);
		}
		else if(drawBodyPart == 5) {
			g.drawLine(47,56,47,92);
		}
		else if(drawBodyPart == 4) {
			g.drawLine(47,68,35,68);
		}
		else if(drawBodyPart == 3) {
			g.drawLine(47,68,59,68);
		}
		else if(drawBodyPart == 2) {
			g.drawLine(47,92,38,100);
		}
		else if(drawBodyPart == 1) {
			g.drawLine(47,92,56,100);
		}
	}

	/**
	 *  returns true if letter inputed is in the word.		
	 *  <br>
     *  pre: word != null </br>
     *  post: none
     *  @param word current word; letter current user's guess
     *  @return true if the word contains the current guess, false otherwise
     */
	public static boolean compare(String word, char letter) {
		if(word.contains(letter+"")) {
			return true; 
		}
		return false;
	}

	/**
	 *  This method creates a new game if the user agrees
	 *  <br>
     *  pre: sc != null </br>
     *  post: none
     *  @param sc Scanner
     */
	public static void askForNewGame(Scanner sc) throws IOException {
		System.out.println("\nEnter \"y\" to play another game. Anything else to quit.");
		String answer = sc.next();
		if(answer.equals("y")) {
			DrawingPanel canvas = new DrawingPanel(100,150);
			String word = pickWord("dictionary.txt");
			introduction(word, sc, canvas);
		}
	}
}
